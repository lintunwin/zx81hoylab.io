---
layout: post
title:  "Jackpot"
img: "/assets/2022/11/Jackpot.png"
tags: ["2022", "Richard Turnnidge"]
category: "Richard Turnnidge"
---

Una máquina tragaperras para el ZX81.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Primer juego del autor en ensamblador. Las instrucciones necesarias se van mostrando en pantalla.
Empiezas con 10 libras. Cada giro cuesta 1 libras. Cuando pierdes todo el dinero termina la partida.
Si superas las 99 libras, ganas.

Con cualquier par ganas 2 libras. Con un trío ganas 6 libras. Por cada "BAR" ganas una libra.

[Enlace al foro Sinclair ZX World](https://sinclairzxworld.com/viewtopic.php?p=49297&sid=310e55c205860675d90e16fc1d963655#p49297){:target="_blank"} o al [repositorio de github](https://github.com/richardturnnidge/ZX81_Jackpot/tree/main/p_files){:target="_blank"} para descarga el juego.
