---
layout: post
title:  "Valley of the Kings"
month: "MAY"
img: "/assets/2022/05/valleyOfTheKings.jpg"
tags: ["2022", "Bukster"]
category: "Bukster"
---

Un juego en 3D para ZX81 con 16K.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Este juego tiene tres rondas que consta de tres etapas. Toma el tesoro del faraón, roba sus pollos y cada ronda termina en un desafío único del faraón en el que tienes que vencer al cronómetro para completar una tarea. Si fallas en el desafío, el faraón te arrojará a la arena.

En la pantalla en la esquina superior derecha se muestra un mapa que no ayudara a localizar los elementos del juego, sobre todo cuando están muy lejos.

El autor ha estado trabajando durante más de un año en este juego. Usa un sistema de dibujo en 3D que puede escalar y rotar, para que pueda mirar alrededor de un paisaje en 3D y recoger objetos.

Funcionará con hardware estándar de 16k, pero puede usar un interfaz Chroma y una expansión de sonido Zonx.


Podéis ver el resultado en un vídeo que han subido el autor
[ ](https://www.youtube.com/watch?v=RFcqWB2SE2Y){: .video target="_blank" style="background-image:url(../../../assets/play.png), url('https://img.youtube.com/vi/RFcqWB2SE2Y/0.jpg'"}

[Enlace al foro Sinclair ZX World](https://sinclairzxworld.com/viewtopic.php?p=46892#p46892){:target="_blank"} para descarga el juego, también esta en formato MP3 por si quieres usarlo en un ZX81 real.