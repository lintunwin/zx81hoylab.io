---
layout: post
title:  "Calendario 2023"
month: "JAN"
img: "/assets/2022/12/calendario2023.png"
tags: ["2023", "calendario"]
---

[Como el año pasado](https://zx81hoy.gitlab.io/2021/12/30/calendario), he creado un calendario para poner en una caja de cassette con juegos del ZX81 que han salido en el 2022

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Hay dos versiones disponibles: 
* Lunes a domingo en español
* Domingo a sábado en ingles

{:refdef: style="text-align: center;"}
![Calendario](/assets/2022/12/calendario2023_fisico.png)
{: refdef}

[Lunes a domingo en español](/assets/2022/2023_es.pdf){:target="_blank"}

[Domingo a sábado en ingles](/assets/2022/2023_en.pdf){:target="_blank"}
