---
layout: post
title:  "Cross Snake"
month: "DIC"
img: "/assets/2021/12/crosssnake.jpg"
tags: ["2021", "Fabrizio"]
category: "Fabrizio"
---

Versión experimental del Snake para ZX81 

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

El autor de [Cross-Lib](https://github.com/Fabrizio-Caruso/CROSS-LIB/){:target="_blank"}, una librería en C que puede usar para hacer juegos para 200 sistemas retro, escribió este juego y lo compilo para ZX81 con 32K de expansión y con gráficos en alta resolución.

{:refdef: style="text-align: center;"}
![Instrucciones](/assets/2021/12/crosssnake_ins.jpg)
{: refdef}

Los controles son 
* I = arriba
* K = abajo
* J = izquierda
* L = derecha
* Espacio = Disparo.

El juego consiste en coger todas las manzanas de los 32 niveles (existe un nivel secreto) y evitar chocar la cabeza de la serpiente con las minas o el cuerpo de la serpiente. La lectura del teclado no es muy buena, por lo que cuesta un poco cambiar de dirección muy rápido, pero como no mueres al chocar con los muros se compensa esa deficiencia.

[Enlace al juego](https://spectrumcomputing.co.uk/entry/37627/ZX81/Cross_Snake){:target="_blank"}
para su descarga.