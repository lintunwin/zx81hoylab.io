---
layout: post
title:  "Invincible Island"
month: "MAR"
img: "/assets/2022/03/invincible_island.jpg"
tags: ["2022", "Mancave Software"]
category: "Mancave Software"
---

Port de la aventura de texto (en ingles) de 1983 de Peter Cook para ZX Spectrum.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Primer juego creado por [Mancave Softwar](https://mancave.itch.io/){:target="_blank"} y publicado usando su propio motor, MATE (Mancave Adventure Text Engine), inicialmente sólo para ZX81 con 16K, aunque el autor espera adaptarlo a otros sistemas. Según explica en la página de itchio, tardo dos tarde crear el juego, gracias al motor MATE.

Por las limitaciones de memoria, existen algunas diferencias entre esta versión y la versión original de Spectrum de 48k, esta explicado en una archivo de texto descargable en la página de itchio, cuidado que tiene spoilers. Aún con las diferencias, todas las ubicaciones con sus descripciones y casi todos los objetos están presentes. Según el autor, al 95% del juego original.

Aquí está la publicidad del lanzamiento original:

ISLA INVENCIBLE es el primitivo hogar ancestral de la misteriosa tribu Xaro. Al margen de la civilización, la leyenda cuenta que estos salvajes de la isla guardan los tesoros inimaginables de su primer gran líder, el propio Xaro.

Afortunadamente, tienes algo más que una leyenda para guiarte. Mientras estaba en la Isla Invencible, el Dr. Chumley, el mundialmente famoso explorador, envió un último mensaje desesperado en una botella. Entre sus súplicas de ayuda y rescate había una pista vital: "Primero encuentra los Siete Pergaminos de Xaro". Nunca más se supo de él, ¡quizás los nativos tenían hambre!

Armado con esta información, tu misión es encontrar los siete pergaminos que, si el Dr. Chumley tenía razón, deberían llevarte al tesoro. Pero recuerda: el tesoro no te sirve de nada a menos que puedas escapar con él, ¡vivo!

[Enlace a itchio](https://mancave.itch.io/invincible-island){:target="_blank"} para su descarga.