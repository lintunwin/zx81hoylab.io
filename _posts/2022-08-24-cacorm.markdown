---
layout: post
title:  "Cacorm"
month: "AUG"
img: "/assets/2022/08/cacorm.png"
tags: "2022 Inufuto"
category: Inufuto
---

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Tienes que eliminar todos los círculos rodeandolos con la estela que dejas al moverte y evitar a los monstruos.
Cuando eliminas todos los círculos pasa a la siguiente pantalla, debes de cerrar la estela para que elimine los círculos que hay dentro de ella. 

Los enemigos no pueden pasar por la estela pero tampoco los eliminas si cierras la estala con ellos dentro.
Tampoco puedes pasar tu personaje atraves de la estela.

Tienes tiempo en cada fase, si acaba el tiempo pierdes una vida.

Podéis ver el juego en un vídeo que han subido el autor
[ ](https://www.youtube.com/watch?v=72ZplBcHhzw){: .video target="_blank" style="background-image:url(../../../assets/play.png), url('https://img.youtube.com/vi/72ZplBcHhzw/0.jpg'"}

Los controles son las siguiente teclas
* Q = arriba
* A = abajo
* O = izquierda
* P = derecha
* Espacio o Enter = empezar partida

[Enlace a la web del autor](http://inufuto.web.fc2.com/8bit/cacorm/#zx81){:target="_blank"} donde puedes descargar el juego en formato P o wav para cargar en una máquina real.

Además tiene el juego para muchos otros sistemas.

[Enlace directo al juego](http://inufuto.web.fc2.com/8bit/zx81/cacorm.p "download"){:target="_blank"}
