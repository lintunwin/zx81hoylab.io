---
layout: post
title:  "Robbers of the Lost Tomb"
month: "DIC"
img: "/assets/2022/04/robbers_of_the_lost_tomb.jpg"
tags: "2022 traducciones"
tags: ["2022", "traducciones", "Timeworks"]
category: "Timeworks"
---

He traducido el juego [Robbers of the Lost Tomb de 1982](http://www.zx81stuff.org.uk/zx81/tape/RobbersOfTheLostTomb){:target="_blank"} al español.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

El juego consiste en buscar cuatro tablillas sagradas a través de las, muchas, habitaciones de una tumba perdida mientras intentas evitar momias, serpientes y fantasmas.

En cada habitación, se le presentan tres puertas numeradas y tiene la opción de lanzar un cuchillo o moverse a través de una puerta. También se le informará si hay algún peligro cerca.

Si encuentra algún fantasma, lo trasladarán a otra parte de la tumba, pero las momias y las serpientes deben evitarse o arrojarles un cuchillo para matarlas. Antes de la misión, puedes seleccionar tu nivel de habilidad (1-10) y si quieres que las momias se muevan por la tumba. En ciertos niveles de habilidad, puedes decidir cuántos cuchillos llevar y cuántas momias y peligros quieres en la tumba.

[Enlace al juego](https://gitlab.com/salvacam/zx81/-/blob/master/robbers_of_the_lost_tomb/robbersofthelosttomb_es.p) para su descarga.