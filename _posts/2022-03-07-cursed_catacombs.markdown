---
layout: post
title:  "Cursed Catacombs"
month: "MAR"
img: "/assets/2022/03/cursed_catacombs.jpg"
tags: ["2022", "Walter Rissi"]
category: "Walter Rissi"
---

Juego arcade en 16K, donde tienes que recoger tesoros evitando a los guardianes.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

A través del grupo de facebook [ZX81 Owners Club](https://www.facebook.com/groups/ZX81OwnersClub/){:target="_blank"} ha compartido el usuario Walter su primer juego en ensamblador para ZX81. Espero que se el primero de muchos, en el grupo de facebook tiene capturas de otros juegos en desarrollo.

El juego consiste en recoger todos los tesoros de cada una de las 10 salas que tiene el juego. Evitando a los guardianes de la tumba. 
Hay cuatro guardianes con diferente patrones de movimiento.

Si para ti no es suficiente el nivel fácil tienes 5 niveles de dificultad.

Dispones de tres vidas por partida, si pierdes un vida tienes volver a coger todos los tesoros de la tumba en la que te encuentres.

Se usan las teclas opqa o los cursores para mover al jugador


[Enlace de descarga](https://drive.google.com/file/d/1KllUQ7KKyrBhZyeEBa8TwJ7mHAlwBjKi/view){:target="_blank"}

**Editado el 29-05-2022**

El autor ha publicado una nueva versión de **Cursed Catacombs**, que es un poco más fácil en los últimos 3 niveles que eran casi imposibles de terminar.

[Enlace de descarga a la actualización](https://drive.google.com/drive/folders/1piiSF6yiliQNao1UUreAB_RQ24oylZs3?fbclid=IwAR3MHhfmfLQq_YSkHNyrIz8FEi5Tuo6mamRO4gT3L9kOKbxUM1f_b-QUfH4){:target="_blank"}