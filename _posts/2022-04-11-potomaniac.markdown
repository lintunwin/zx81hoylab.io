---
layout: post
title:  "Potomaniac"
month: "APR"
img: "/assets/2022/04/potomaniac.jpg"
tags: ["2022", "XavSnap"]
category: "XavSnap"
---

Un Game&Watch donde tienes recoger cerveza.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

En abril de 1983 una fábrica de cerveza alemana (controlada por un ZX81) explotó!

Un miembro del grupo de superheroes del Zxteam intenta salvar la preciosa cerveza que llueve..."

Tienes que recoger toda la cerveza posible con cuidado de no beber más de 5 vasos segidos sin pasar por el baño, a la izquierda de la pantalla.

Para mover se usan las teclas 5 y 8, para izquierda y derecha.

Solo tienes una vida que se pierde si bebes más cerveza de lo que puede aguantar (5 vasos), se puede caer la cerveza al suelo sin perder ni vidas ni el juego.
{:refdef: style="text-align: center;"}
![game over](/assets/2022/04/potomaniac_gameover.jpg)
{: refdef}

El juego esta escrito en Basic, para usar con una expansión de 16 kb, en el foro donde se ha presentado esta una captura del código.

[Enlace al foro Sinclair ZX World](https://sinclairzxworld.com/viewtopic.php?p=46344#p46344){:target="_blank"} para descarga el juego.