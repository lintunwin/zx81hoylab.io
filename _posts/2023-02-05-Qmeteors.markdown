---
layout: post
title:  "Qmeteors"
month: "FEB"
img: "/assets/2023/02/Qmeteor.png"
tags: ["2023", "Paul Daniels"]
category: "Paul Daniels"
---

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Viaja a través de la tormenta de meteoritos, intentando llevar suministros a la colonia. Una vez que llegues, deberás regresar.

Dentro de la tormenta, busque restos antiguos (*) que se pueden recolectar para obtener más puntos.

También se pueden encontrar artefactos (B) dejados por una raza alienígena que se pueden usar para destruir todos los meteoros en el rango.

Los controles son las siguientes teclas:
* Q - T = Izquierda
* Y - P = Derecha
* B - Space = Bomba

[Enlace al foro Sinclair ZX World](https://sinclairzxworld.com/viewtopic.php?p=49715#p49715){:target="_blank"} para descarga el juego.
