---
layout: post
title:  "2PAXMAN"
month: "NOV"
img: "/assets/2021/11/2paxman.jpg"
tags: ["2021", "Dr Beep"]
category: "Dr Beep"
---

Un clon de pacman para el ZX81

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Inicialmente llamado ZPAXMAN, una combinación de ZX y PACMAN, fue modificado el nombre para hacer posibles 2 juegos de pacman.

Cada juego tiene 4 niveles para jugar.

El juego tiene otro juego oculto, que se desbloquea cuando tienes la puntuación más alta en los dos juegos, podras seleccionar el juego 3 que tiene otros 4 niveles más.
Al terminar una partida jugando a los 3 modos de juegos se agrega la opción 3 al menú.

{:refdef: style="text-align: center;"}
![3PAXMAN menu](/assets/2021/11/2paxman_menu.jpg)
{: refdef}

La única pega es que no permite mantener pulsada dos teclas si quieres cambiar de dirección tienes que dejar de pulsar la última tecla y pulsar la nueva, y para los cambios rápidos de dirección cuesta un poco acostumbrarse

[Enlace al foro Sinclair ZX World](https://www.sinclairzxworld.com/viewtopic.php?p=41373#p41373){:target="_blank"}
donde hay más detalles y el enlace de descarga.
