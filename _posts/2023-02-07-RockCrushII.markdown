---
layout: post
title:  "Rock Crush II"
month: "FEB"
img: "/assets/2023/02/rc2.png"
tags: ["2023", "Steven McDonald"]
category: "Steven McDonald"
---

Más de 30 años después la secuela del juego Rock Crush de 1985.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Es un nuevo motor de juego con 80 cuevas únicas.

Diamond Dan tiene nuevas habilidades, potenciadores, bombas, espadas y arañas mutantes gigantes. 

Al cargar, selecciona en cuál de los 4 diferentes sistemas de cuevas jugarás.

Cada sistema tiene 20 cuevas divididas en 4 secciones. Solo se puede elegir una vez, si quieres jugar otra sección tienes que volver a cargar el juego.

El objetivo es recoger todos los diamantes y dirigirte a la salida de la siguiente cueva.

El juego necesita 16K, es compatible con el interfaz Chroma, sonido y un joystick a través de las interfaces Chroma o ZXpand.

Los controles son las siguientes teclas:
* Q = arriba
* Z = abajo
* I = izquierda
* P = derecha
* Enter = nuevo juego
También funcionan las teclas del cursor.

[Enlace a la versión digital](https://rockcrush.onlineweb.shop/product/rock-crush-ii-the-power-of-dan-digital-version-2022){:target="_blank"} el juego vale 2 libras, pero el autor ha puesto 100 unidades gratuitas.
