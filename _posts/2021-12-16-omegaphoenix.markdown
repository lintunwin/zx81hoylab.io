---
layout: post
title:  "Omega Phoenix"
month: "DIC"
img: "/assets/2021/12/omegaphoenix.jpg"
tags: ["2021", "Bukster"]
category: "Bukster"
---

Conversión del arcade de 1980 Phoenix

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Es necesario una expansión de 16k, tiene soporta para el interfaz Chroma y Zon-X81

El juego sigue el patrón del juego arcade original, dos ataques de pájaros pequeños seguidas de dos ataques de pájaros grandes, que salen de sus huevos, y una batalla con el jefe.

Se obtiene una vida extra cada 10.000 puntos

{:refdef: style="text-align: center;"}
![Omega Phoenix Chroma](/assets/2021/12/omegaphoenix_chroma.jpg)
{: refdef}

Al tener sonido y colores es una conversión muy lograda y casí da la sensación de estar jugando al arcade original


[Enlace de descarga](http://bukstergames.bizhat.com/){:target="_blank"} de la web del autor