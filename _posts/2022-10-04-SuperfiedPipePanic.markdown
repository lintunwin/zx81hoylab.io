---
layout: post
title:  "Superfied Pipe Panic"
img: "/assets/2022/10/superfiedPipePanic.png"
tags: "2022 Reboot"
category: "Reboot"
---

[Juego original de Thunor escrito en el 2010](https://www.sinclairzxworld.com/viewtopic.php?f=4&t=339){:target="_blank"}

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Ahora Reboot le ha agregado soporte de sonido y joystick ZXpand+.

El juego consisten en unir distintas piezas de tubería para conectar la entrada con la salida antes de que se agote el tiempo y el agua salga de la tubería de salida.

Los controles son definibles, por defecto son las siguientes teclas
* Q = arriba
* A = abajo
* O = izquierda
* P = derecha
* K = poner tubería 

[Enlace al foro Sinclair ZX World](https://www.sinclairzxworld.com/viewtopic.php?f=11&t=4819){:target="_blank"} para descarga el juego.