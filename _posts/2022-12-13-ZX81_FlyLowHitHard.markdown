---
layout: post
title:  "ZX81 Fly Low Hit Hard"
month: "JUL"
img: "/assets/2022/12/fly.png"
tags: ["2022", "Adrian Pilkington"]
category: "Adrian Pilkington"
---

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Un juego de naves con scroll lateral similar al Defender.

Podéis ver el resultado en un vídeo que han subido el autor
[ ](https://www.youtube.com/watch?v=wCblFYSnBCQ){: .video target="_blank" style="background-image:url(../../../assets/play.png), url('https://img.youtube.com/vi/wCblFYSnBCQ/0.jpg'"}

[Código fuente del juego](https://github.com/AdrianPilko/zx81-fly-low-hit-hard-game){:target="_blank"}

Se puede jugar con joystick o teclado. Los controles son para teclado son:
* X = arriba
* N = abajo
* Espacio = disparar

Se pueden descargar los juegos del [repositorio de github](https://github.com/AdrianPilko/zx81-fly-low-hit-hard-game/blob/main/fly.p){:target="_blank"}
o en [itchio](https://adrianpilko.itch.io/fly-low-hit-hard){:target="_blank"}