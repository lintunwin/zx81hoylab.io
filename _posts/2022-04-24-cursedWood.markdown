---
layout: post
title:  "The Cursed Woods"
month: "APR"
img: "/assets/2022/04/cursedWoods.jpg"
tags: ["2022", "Christoph & Julia"]
category: "Christoph & Julia"
---

¿Seras capaz de derrotar a Chronos?

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Un juego realizado por varios integrantes del [ZX-Team alemán](http://www.zx81.de/english/_frame_e.htm){:target="_blank"}. 
Escrito en basic y compilado con [Mcoder II](http://www.zx81stuff.org.uk/zx81/tape/MCoder11){:target="_blank"}

El juego es una mezcla de aventuras y lucha. Empiezas moviendo tu personaje por un mapa, donde tienes que recoger 30 cristales, puedes aumentar tu poder cogiendo + y los T son los enemigos, en los arbustos puede encontrar enemigos.

Cuando encuentras un enemigo tienes que luchar contra el hasta que acabes con él, o el acabe contigo.

{:refdef: style="text-align: center;"}
![lucha](/assets/2022/04/cursedWoods_fight.jpg)
{: refdef}

Una vez tengas los 30 cristales tendrás que luchar contra el enemigo final.

Es necesaria una expansión de memoria de 16K, los controles son  
* Q = arriba
* A = abajo
* O = izquierda
* P = derecha
* 0 = disparo

[Enlace al foro Sinclair ZX World](https://sinclairzxworld.com/viewtopic.php?p=46317#p46317){:target="_blank"} para descarga el juego.