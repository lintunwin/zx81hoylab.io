---
layout: post
title:  "Dollar Race y Chase Race"
month: "DIC"
img: "/assets/2022/12/dollarRace.png"
tags: ["2022", "Kurt-Arne Johnsen"]
category: "Kurt-Arne Johnsen"
---

Dos nuevos juegos que han aparecido en el grupo de facebook [ZX81 Owners Club](https://www.facebook.com/groups/ZX81OwnersClub/about){:target="_blank"}

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

**Dollar Race**

Un sencillo juego basado en un programa, Minipede de G. Creasy, en la revista Sinclair Programs en mayo 1983.
Tienes que ir cogiendo el dinero y evitando chocar con las paredes a medida que vas bajando.

Los controles son las siguientes teclas:
* O = izquierda
* P = derecha

El juego está en github en formato p.

[Repositorio de github](https://github.com/Retronissen/Sinclair-ZX81-Software-in-hounor-for-Sir-Clive){:target="_blank"}
para su descarga.

[Enlace directo](https://github.com/Retronissen/Sinclair-ZX81-Software-in-hounor-for-Sir-Clive/blob/main/DollarRace.p?raw=true){:target="_blank"} al archivo .p

{:refdef: style="text-align: center;"}
![Chase Race](/assets/2022/12/chaseRace.png)
{: refdef}

**Chase Race**

Manejas la H que está en la parte superior de la pantalla y tienes que alcanzar el bloque negro que está en la parte superior de la pantalla. Debes evitar chocar con las paredes, tanto las de los lados como las que van apareciendo en el centro, si coges los dos puntos ganas 10 puntos, si coges un dólar ganas 1 punto y bajas una línea más. Si chocas con una pared pierdes la partida. 

Los controles son las siguientes teclas:
* O = izquierda
* P = derecha

[Repositorio de github](https://github.com/Retronissen/Sinclair-ZX81-Software-in-hounor-for-Sir-Clive){:target="_blank"}
para su descarga.

[Enlace directo](https://github.com/Retronissen/Sinclair-ZX81-Software-in-hounor-for-Sir-Clive/blob/main/ChaseRace.p?raw=true){:target="_blank"} al archivo .p