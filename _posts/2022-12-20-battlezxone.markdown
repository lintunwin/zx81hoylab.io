---
layout: post
title:  "Battlezxone"
month: "DIC"
img: "/assets/2022/12/battlezone.png"
tags: ["2022", "Bukster"]
category: "Bukster"
---

Versión del arcade [Battlezone](https://es.wikipedia.org/wiki/Battlezone){:target="_blank"}

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Ya hace meses el autor de este juego realizó un juego en 3D, [Valley of the Kings](https://zx81hoy.gitlab.io/2022/05/29/valleyOfTheKings). Usando como base ese juego ha creado una versión del arcade Battlezone.

En esta versión los tanques no se mueven ni disparan. Solo tienes como amenaza los misiles guiados. En el nivel de dificultad uno te dispararán un misil, si eliges el nivel de dificultad dos, te dispararán dos misiles a la vez.

El nivel termina cuando se destruyen todos los tanques. Si destruyes los cactus conseguirás puntos extras.

Los controles son redefinibles por defecto son:
* Q = arriba
* A = abajo
* O = derecha
* P = izquierda
* 0 = disparar

Funcionará con hardware estándar de 16k, pero puede usar un interfaz Chroma y una expansión de sonido Zonx.

[Enlace al foro Sinclair ZX World](https://sinclairzxworld.com/viewtopic.php?p=49480#p49480){:target="_blank"} para descarga el juego.
