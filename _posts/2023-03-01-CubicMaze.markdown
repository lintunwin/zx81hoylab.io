---
layout: post
title:  "CUBICMAZE"
month: "Mar"
img: "/assets/2023/03/Cubicmaze.png"
tags: ["2023", "Dr Beep"]
category: "Dr Beep"
---

Un nuevo juego de un 1K

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Cuando empiezas un nuevo laberinto tienes que visitar todas las zonas de ese cubo para resolver el cubo. 

En una "U" puedes subir un piso. 
En una "D" puedes bajar un piso. 
En una "H", escalera, puedes subir y bajar.

En el lado se muestra el piso en el que estás.
En la parte inferior izquierda, se muestran las coordenadas en la que te encuentras. 
En la parte inferior derecha se ve el número de zonas que aún no has visitado. 

Los controles se muestran al cargar el juego. Se puede redefinir las teclas pulsando Y al cargar el juego.
Si pulsa cualquier otra tecla se inicia el juego.

El juego empieza con un laberinto de 1x1x1. Hay que pulsar la tecla salida (X si no has redefinido otra) para terminar este laberinto. Ahora puedes seleccionar un nuevo laberinto pulsando del 1 al 6.

1 para un laberinto de 1x1x1 (1 zona)

2 para un laberinto de 2x2x2 (8 zonas)

3 para un laberinto de 3x3x3 (27 zonas)

4 para un laberinto 4x4x4 (64 zonas)

5 para un laberinto de 5x5x5 (125 zonas)

6 para un laberinto de 6x6x6 (216 zonas)

Cuando salga del laberinto después de visitar todos los campos, el juego mostrará SOLVED y podrás seleccionar un nuevo laberinto.

[Enlace al foro Sinclair ZX World](https://sinclairzxworld.com/viewtopic.php?p=50161#p50161){:target="_blank"} para descarga el juego.






