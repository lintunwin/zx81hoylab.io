---
layout: post
title:  "Calendario 2022"
month: "DIC"
img: "/assets/2021/12/calendario.jpg"
tags: ["2022", "calendario"]
---

He creado un calendario para poner en una caja de cassette con programas del ZX81 que han salido en el 2021

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Hay dos versiones disponibles: 
* Lunes a domingo en español
* Domingo a sábado en ingles

{:refdef: style="text-align: center;"}
![Calendario](/assets/2021/12/calendario_fisico.jpg)
{: refdef}

[Lunes a domingo en español](/assets/calendario2021/2021_es.pdf){:target="_blank"}

[Domingo a sábado en ingles](/assets/calendario2021/2021_en.pdf){:target="_blank"}