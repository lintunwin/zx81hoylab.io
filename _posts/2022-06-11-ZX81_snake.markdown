---
layout: post
title:  "ZX81 Snake"
month: "JUN"
img: "/assets/2022/06/zx81Snake.png"
tags: ["2022", "Adrian Pilkington"]
category: "Adrian Pilkington"
---

Un reboot del juego de la serpiente para ZX81.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}


El juego consiste en manejar una serpiente y en ir comiendo manzanas, a medida que nos comemos una manzana la serpiente crece, en la pantalla tenemos a la izquierda el número de manzanas comidas (en Score) y a la derecha la longitud de la serpiente. Podemos tocar las paredes exteriores pero hay que evitar tocar la pared central, si la tocamos el juego terminara.


Podéis ver el resultado en un vídeo que han subido el autor
[ ](https://www.youtube.com/watch?v=ySS7uNfF3-s){: .video target="_blank" style="background-image:url(../../../assets/play.png), url('https://img.youtube.com/vi/ySS7uNfF3-s/0.jpg'"}

[Código fuente del juego](https://github.com/AdrianPilko/ZX81-1K-Experiments/blob/main/snake16K.asm){:target="_blank"}

Los controles son  
* Z = izquierda
* M = derecha
* X = arriba
* N = abajo
* S = empezar

Se pueden descargar los juegos del [repositorio de github](https://github.com/AdrianPilko/ZX81-1K-Experiments/blob/main/snake16K.p){:target="_blank"}
o en [itchio](https://adrianpilko.itch.io/zx81-snake-game){:target="_blank"}