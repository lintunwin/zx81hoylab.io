---
layout: post
title:  "ZX Adventure"
month: "AUG"
img: "/assets/2022/08/zxAdventure.png"
tags: ["2022", "Steven Goodwin"]
category: "Steven Goodwin"
---

Un juego de aventura inspirado en el clásico de Atari 2600.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

ZX Adventure está inspirado en el clásico juego Atari 2600 del mismo nombre.

Es un juego publicado en 2021. En el que debes encontrar el cáliz y llevarlo a la sala del trono. Debes evitar chocar con los dragones (símbolo >),  puedes acabar con los dragones si llevas la espada (T).
Necesitas encontrar las llaves, con su correspondiente, para abrir las puertas.
Solo puedes llevar un solo objeto a la vez.

Los controles son los cursores o las siguiente teclas
* K  = arriba
* M = abajo
* Z = izquierda
* X = derecha


El autor tiene en su blog una [versión del juego para ZX Spectrum](http://marquisdegeek.com/code_zxadventure){:target="_blank"}. La versión de ZX81 es un poco más extensa que la de ZX Spectrum.

{:refdef: style="text-align: center;"}
![title](/assets/2022/08/zxAdventure_title.png)
{: refdef}

El juego puede ser comprado en la [web de Cronosoft](https://cronosoft.fwscart.com/){:target="_blank"} en [formato físico](https://cronosoft.fwscart.com/ZX_ADVENTURE_-_Sinclair_ZX81_16K_RAM_on_cassette/p5357732_20954618.aspx){:target="_blank"} por 7 libras (más gastos de envío) o en [formato digital](https://cronosoft.fwscart.com/ZX81_DIGITAL_VERSION_for_emulator/p5357732_21597228.aspx){:target="_blank"}  por 1,99 libras.