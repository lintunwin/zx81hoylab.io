---
layout: post
title:  "Wall-Run"
month: "DIC"
img: "/assets/2022/12/wallRun.png"
tags: ["2022", "Orac81"]
category: "Orac81"
---

Un juego para ZX80 y ZX81, con versión de 1k o 2k.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Tienes que controlar tu pared y atrapa al oponente de la computadora. Cuando lo consigues, avanzas al siguiente nivel.
Tienes que atrapar la computadora dos veces en el nivel 2, 3 veces en el 3, etc.
Si recoges dólares obtienes puntos, en niveles más altos obtienes más puntos.
La versión 2k tiene una pantalla más grande, puntuación/puntuación más alta y pantalla de título.

El número de nivel se muestra en la línea superior e inferior.

Los controles son:
* W = moverte arriba
* A = moverte izquierda
* D = moverte derecha
* X = moverte abajo

[Enlace al foro Sinclair ZX World](https://sinclairzxworld.com/viewtopic.php?p=49540#p49540){:target="_blank"} para descarga el juego.
