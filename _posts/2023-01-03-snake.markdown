---
layout: post
title:  "Snake"
month: "JAN"
img: "/assets/2023/01/snake.png"
tags: ["2023", "Steven Reid"]
category: "Steven Reid"
---

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

La versión de Snake para ZX81 del autor, Steven Reid, en basic.

Tienes que evitar chocar con los muros del borde de la pantalla y con el cuerpo de la serpiente a medida que vas comiendo los elementos que hay en la pantalla.

Los controles son:
* W = arriba
* S = abajo
* A = izquierda
* D = derecha

El código se puede consultar en este [link](https://www.reids4fun.com/zl/snake){:target="_blank"}

En [esta entrada](https://www.reids4fun.com/536/lets-have-fun-playing-snake-on-the-zx81){:target="_blank"} de su blog explica todo el proceso de creación

Puedes [jugar online](https://www.reids4fun.com/zp/snake){:target="_blank"} o [descargar el juego](https://www.reids4fun.com/programs/snake.p){:target="_blank"}
