---
layout: post
title:  "Zedi Blaster"
month: "JUL"
img: "/assets/2022/07/zediBlaster.png"
tags: ["2022", "Jonathan Cauldwell"]
category: "Jonathan Cauldwell"
---

Un Bomberman para ZX81.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Como no existian ninguna versión de Bomberman para ZX81 Jonathan Cauldwell ha decidido hacer una.

El objetivo es matar a todos los enemigos con tus bombas y llegar a la salida, que esta oculta en alguno de los bloques que se pueden romper con la explosión. También puedes encontrar bonus, como mayor explosión de tus bombas o posibilidad de poner más bombas simultaneamentes (al principio puedes poner dos a la vez)

Tienes que evitar que te toquen los enemigos y la explosión de tus bombas, si no perderás una vida de las 3 que tienes y tendrás que volver desde el inicio del nivel.

En este juego no tienes tiempo y si ya ha explotado la bomba no puedes pasar por la explosión, dos factores que hacen más accesible el juego.

El juego es compatible con el interfaz Chroma, joystick ZXPAND y expansión de caracteres QS.

{:refdef: style="text-align: center;"}
![chroma81](/assets/2022/07/zediBlaster_chroma.png)
{: refdef}

El juego puede ser comprado en la [web de Cronosoft](https://cronosoft.fwscart.com/){:target="_blank"} en [formato físico](https://cronosoft.fwscart.com/product/zx81-16k--zedi-blast-cassette-tape){:target="_blank"} por 7 libras (más gastos de envío) o en [formato digital](https://cronosoft.fwscart.com/ZX81_DIGITAL_VERSION_for_emulator/p5357732_21597228.aspx){:target="_blank"}  por 1,99 libras.