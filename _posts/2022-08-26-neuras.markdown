---
layout: post
title:  "Neuras"
month: "AUG"
img: "/assets/2022/08/neuras.png"
tags: "2022 Inufuto"
category: Inufuto
---

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Tienes que coger una carta, hasta que no la coges no sabrás que carta es, se muestra en la parte superior derecha.
Tienes que tirar, pulsando espacio, y si colisiona con otra carta igual (con el mismo número o letra) desaparecerán el par de cartas.

Tienes tiempo en cada fase, si acaba el tiempo pierdes una vida. Si te toca un monstruo pierdes una vida.
Si pierdes una vida en una pantalla solo tienes que hacer desaparecer las cartas que te queden por juntar, y si llevaras una carta sigues llevando.

Puedes usar la carta que lleva para lanzarla a los monstruos y dejarlo aturdidos unos segundos. Mientras estén aturdidos no se mueven y puedes pasar sobre ellos.

Podéis ver el juego en un vídeo que han subido el autor
[ ](https://www.youtube.com/watch?v=myV0BUrN1Js){: .video target="_blank" style="background-image:url(../../../assets/play.png), url('https://img.youtube.com/vi/myV0BUrN1Js/0.jpg'"}

Los controles son las siguiente teclas
* Q = arriba
* A = abajo
* O = izquierda
* P = derecha
* Espacio = disparar carta
* Espacio o Enter = empezar partida

[Enlace a la web del autor](http://inufuto.web.fc2.com/8bit/neuras/#zx81){:target="_blank"} donde puedes descargar el juego en formato P o wav para cargar en una máquina real.

Además tiene el juego para muchos otros sistemas.

[Enlace directo al juego](http://inufuto.web.fc2.com/8bit/zx81/neuras.p "download"){:target="_blank"}

