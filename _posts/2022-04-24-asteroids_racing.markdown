---
layout: post
title:  "ZX81 Asteroids y ZX81 Racing"
month: "APR"
img: "/assets/2022/04/asteroids.jpg"
tags: ["2022", "Adrian Pilkington"]
category: "Adrian Pilkington"
---

Dos nuevos juegos para 16k.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

**ZX81 Asteroid**

Tienes que esquivar las rocas que van hacia ti
[ ](https://www.youtube.com/watch?v=vieoDKWXXCc){: .video target="_blank" style="background-image:url(../../../assets/play.png), url('https://img.youtube.com/vi/vieoDKWXXCc/0.jpg'"}

[Código fuente del juego](https://github.com/AdrianPilko/zx81-asteroids-game){:target="_blank"}


{:refdef: style="text-align: center;"}
![racing](/assets/2022/04/racing.jpg)
{: refdef}

**ZX81**

Un port de ZX Spectrum a ZX81, de un juego de carreras de un antiguo tutorial.
[ ](https://www.youtube.com/watch?v=UBQyVtAhb2U){: .video target="_blank" style="background-image:url(../../../assets/play.png), url('https://img.youtube.com/vi/UBQyVtAhb2U/0.jpg'"}

[Código fuente del juego](https://github.com/AdrianPilko/zx81-racing-game-port){:target="_blank"}

los controles para ambos juegos son  
* Z = izquierda
* M = derecha
* S = empezar

Se pueden descargar los juegos de los repositorios de github
* [ZX81 Asteroid](https://github.com/AdrianPilko/zx81-asteroids-game/raw/main/asteroids.p){:target="_blank"}
* [ZX81 Racing](https://github.com/AdrianPilko/zx81-racing-game-port/raw/main/racing.p){:target="_blank"}