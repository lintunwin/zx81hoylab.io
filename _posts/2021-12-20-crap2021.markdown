---
layout: post
title:  "Crap Games 2021"
month: "DIC"
img: "/assets/2021/12/crap2021.jpg"
tags: ["2021", "concurso"]
---

Este año ha habido 11 juegos para ZX81 presentados en el concurso Crap Games 

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

La 25 edición de este concurso nos ha dejado un total de 62 entradas
* 13 para ZX Spectrum 16K
* 22 para ZX Spectrum 48K
* 6 para ZX Spectrum 128K
* 11 para ZX81
* 2 para ZX80
* 5 para QL 
* 2 para Z88
* 1 para Jupiter Ace


Los juegos presentados para ZX81 son:
1. [DON'T PANIC](https://www.rickdangerous.co.uk/csscgc2021/review014.html){:target="_blank"} 
2. [MOTORACE81](https://www.rickdangerous.co.uk/csscgc2021/review017.html){:target="_blank"}
3. [CAMEL RACING](https://www.rickdangerous.co.uk/csscgc2021/review018.html){:target="_blank"}
4. [ZONKEY KONG](https://www.rickdangerous.co.uk/csscgc2021/review019.html){:target="_blank"}
5. [ZHUNDER VLADE](https://www.rickdangerous.co.uk/csscgc2021/review022.html){:target="_blank"}
6. [SNAIL MAZE](https://www.rickdangerous.co.uk/csscgc2021/review032.html){:target="_blank"}
7. [WALL OF CHINA](https://www.rickdangerous.co.uk/csscgc2021/review034.html){:target="_blank"}
8. [FALL PALO T](https://www.rickdangerous.co.uk/csscgc2021/review046.html){:target="_blank"}
9. [ESCARABAJO](https://www.rickdangerous.co.uk/csscgc2021/review048.html){:target="_blank"}
10. [HIGHWAY ROBBERY 2021](https://www.rickdangerous.co.uk/csscgc2021/review049.html){:target="_blank"}
11. [RUDOLPH PRACTICES](https://www.rickdangerous.co.uk/csscgc2021/review061.html){:target="_blank"}

Son enlaces a la revisión del organizador del concurso.

[Enlace al concurso](https://www.rickdangerous.co.uk/csscgc2021/index.html){:target="_blank"} 