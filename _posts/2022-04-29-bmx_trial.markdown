---
layout: post
title:  "BMX Trial"
month: "APR"
img: "/assets/2022/04/bmxTrial.jpg"
tags: ["2022", "Salvacam"]
category: "Salvacam"
---

Una demake del [Alex Kidd Bmx Trial](https://segaretro.org/Alex_Kidd_BMX_Trial){:target="_blank"} para ZX81

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Mi intención era presentar este juego para el concurso [Bike](https://itch.io/jam/zx-spectrum-game-jam-2021){:target="_blank"}. Pero al final no me dio tiempo de acabarlo.

En este concurso se podían presentar juegos para ZX80, ZX81, ZX Spectrum y Next, en cualquier lenguaje, y de temática relacionada con bicicletas o motocicletas. Quedo desierto, no se llego a presentar ningún juego. 

Aunque lo tenía casi, me faltaron varias cosas por terminar.

{:refdef: style="text-align: center;"}
![pantalla de inicio](/assets/2022/04/bmxTrial_start.jpg)
{: refdef}

El juego consiste en aguantar el máximo tiempo posible pedaleando, sino pedaleas no aumentas la puntuación, cada cierto tiempo van,en sentido contrario, otros ciclistas que tienes que evitar chocar con ellos, y mientras vas pedaleando irán saliendo rocas en el camino que también debes evitar chocar con ellas.
Si pasas por el barro, que esta en los laterales, iras más lento.

Los controles son  
* O = izquierda
* P = derecha
* M o Espacio = pedalear

[Enlace del código.](https://salvacam.gitlab.io/asset/bmxTrial.c){:target="_blank"}

[Enlace del juego.](https://salvacam.gitlab.io/asset/bmxTrial.P){:target="_blank"}

Todos lo juegos que he realizado para ZX81 y ZX Spectrum, además del código fuente, se encuentran en [esta web](https://salvacam.gitlab.io/){:target="_blank"} y se puede probar desde la web (excepto el único juego de ZX80, no he encontrado un emulador de ZX80 en javascript).