---
layout: post
title:  "Zeddytron 2081"
month: "APR"
img: "/assets/2022/04/zeddytron.jpg"
tags: ["2022", "Walter Rissi"]
category: "Walter Rissi"
---

Nuevo juego de Naranjito games,[aquí comente el primero](https://zx81hoy.gitlab.io/2022/03/07/cursed_catacombs).

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

De nuevo, por el grupo de facebook [ZX81 Owners Club](https://www.facebook.com/groups/ZX81OwnersClub/){:target="_blank"} ha compartido el usuario Walter su segundo juego para ZX81. 

Es una versión del clásico arcade Robotron 2084. Permite moverse y disparar simultáneamente en 8 direcciones. A diferencia del original, debes salvar a todos los humanos antes de pasar al siguiente nivel, además de acabar con todos los zeddytrons.

Hay que resistir 99 oleadas, y cada 5 obtienes una vida extra.	

{:refdef: style="text-align: center;"}
![instrucciones](/assets/2022/04/zeddytron_instrucciones.jpg)
{: refdef}

Los controles son  
* W = mover arriba
* S = mover abajo
* A = mover izquierda
* D = mover derecha
* I = disparar arriba
* K = disparar abajo
* J = disparar izquierda
* L = disparar derecha

[Enlace de descarga](https://drive.google.com/file/d/1NCsWJK6oK6qn40YLtp7rHP0EowvvTHRZ/view?fbclid=IwAR1WidFHbOxKIgWBxS4_GasjY3_neUnbTKU7rJTTPd914Ydbq0MZPz8t2z8){:target="_blank"}


**Editado el 29-05-2022**

El autor ha publicado una nueva versión de **Zeddytron 2081** con los siguientes cambios:
- El personaje principal tiene un movimiento un poco más lento, lo que lo hace más fácil de controlar.
- Se incluyó un modo de control OPQAM/Cursor, que permite moverse usando OPQA/Flechas y disparar usando M/0 (al disparar, presione simultáneamente la dirección del disparo y disparará en esa). Soporta 8 direcciones de movimiento/disparo. Si tiene un joystick basado en cursor (por ejemplo, CZ, TK), puede usarlo en este modo. El modo de control anterior (movimiento/disparo simultáneo) sigue siendo compatible.
- Se implementó la compatibilidad con el joystick ZXPand+, aunque aún no se ha probado, es probable que no funcione.

[Enlace de descarga a la actualización](https://drive.google.com/drive/folders/1piiSF6yiliQNao1UUreAB_RQ24oylZs3?fbclid=IwAR3MHhfmfLQq_YSkHNyrIz8FEi5Tuo6mamRO4gT3L9kOKbxUM1f_b-QUfH4){:target="_blank"}