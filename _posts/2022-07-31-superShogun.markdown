---
layout: post
title:  "Super Shogun"
month: "JUL"
img: "/assets/2022/07/superShogun.png"
tags: ["2022", "Dr Beep"]
category: "Dr Beep"
---

Versión mejorada de [SHOGUN1K](https://sinclairzxworld.com/viewtopic.php?p=41493#p41493){:target="_blank"} con un nivel extra de dificultad y una opción para jugar contra otra persona.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}


La versión anterior solo 1 modo de juego que sería el nivel de juego FÁCIL.

Como comenta el autor con optimizaciones fue posible agregar una opción de 2 jugadores y dos niveles de dificultad.


Al iniciar debes elegir el modo de juego, E para fácil, F para jugar con otra persona y H para el nivel difícil.

Hay que presionar la tecla dos veces.

Esto se hace para establecer la aleatoriedad del tablero en el primer bucle.

Los controles son  
* Q = arriba
* A = abajo
* O = izquierda
* P = derecha
* Z = seleccionar

[Reglas del juego](https://sinclairzxworld.com/download/file.php?id=9470){:target="_blank"} en ingles.


[Enlace al foro Sinclair ZX World](https://sinclairzxworld.com/viewtopic.php?p=47482#p47482){:target="_blank"} para descarga el juego.

Actualización 11-12-2022: Añadido caracteres chroma
[Enlace al foro Sinclair ZX World](https://sinclairzxworld.com/viewtopic.php?f=4&t=4895){:target="_blank"} para descarga el juego con caracteres chroma.