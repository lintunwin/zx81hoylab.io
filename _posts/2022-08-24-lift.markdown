---
layout: post
title:  "Lift"
month: "AUG"
img: "/assets/2022/08/lift.png"
tags: "2022 Inufuto"
category: Inufuto
---

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Debes coger todas las banderas evitando a los monstruos. Puedes subir usando los ascensor.

Si coges las dos banderas del mismo tipo seguidas, consigues más puntos.

Tienes tiempo en cada fase, si acaba el tiempo pierdes una vida. Si te toca un monstruo pierdes una vida.
Si pierdes una vida en una pantalla solo tienes que coger las banderas que te queden por recoger.

Podéis ver el juego en un vídeo que han subido el autor
[ ](https://www.youtube.com/watch?v=1Z9cIzjegZ8){: .video target="_blank" style="background-image:url(../../../assets/play.png), url('https://img.youtube.com/vi/1Z9cIzjegZ8/0.jpg'"}

Los controles son las siguiente teclas
* O = izquierda
* P = derecha
* Espacio o Enter = empezar partida

[Enlace a la web del autor](http://inufuto.web.fc2.com/8bit/lift/#zx81){:target="_blank"} donde puedes descargar el juego en formato P o wav para cargar en una máquina real.

Además tiene el juego para muchos otros sistemas.

[Enlace directo al juego](http://inufuto.web.fc2.com/8bit/zx81/lift.p "download"){:target="_blank"}

