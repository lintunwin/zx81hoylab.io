---
layout: post
title:  "Retro Snake"
month: "OCT"
img: "/assets/2021/10/retro-snake.jpg"
tags: ["2021", "ZX81ultra"]
category: "ZX81ultra"
---

Otro juego con gráficos en alta resolución pero este para 16k.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Es el típico juego de la serpiente, pero con el añadido de que tienes un enemigo que se va moviendo por la pantalla cambiando de dirección al chocar con las paredes o con la cola de la serpiente y que la distribución de la pantalla va cambiando en cada fase. Al principio solo hay un enemigo, en las pantallas finales salen dos enemigos a la vez.

Tienes que evitar chocar la cabeza de la serpiente con algún enemigo, con tu cuerpo o con alguna pared. Un elemento importante es saber jugar con el cuerpo de la serpiente para dirigir al enemigo.

En cada fase tienes que recoger 10 manzanas y una última, que siempre esta en la el centro de la parte superior, y que sirve de puerta para pasar a la siguiente fase. 

Al principio solo tienes una pared en el centro de la pantalla y a medida que avanzas de fase se va complicando con una distribución más compleja, terminando en la últimas pantallas con paredes móviles.

Bastante difícil de acabar pero asequibles en las primeras pantallas.

[Enlace de descarga](https://www.sinclairzxworld.com/download/file.php?id=7952)

[Enlace al foro Sinclair ZX World](https://www.sinclairzxworld.com/viewtopic.php?p=37645#p37645){:target="_blank"}
donde hay más detalles.
