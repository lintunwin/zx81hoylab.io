---
layout: post
title:  "Bouncing Dots"
month: "FEB"
img: "/assets/2023/02/BounceDot.png"
tags: ["2023", "Dr Beep"]
category: "Dr Beep"
---

Otro juego de 1K de Johan "Dr Beep" Koelman

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Un juego en el que debes capturar 12 puntos en movimiento en una pantalla.
El menor tiempo es el que se mantiene como el mejor tiempo.

Los controles son las siguientes teclas:
* Q = arriba
* A = abajo
* O = izquierda
* P = derecha
* Enter = nuevo juego


[Enlace al foro Sinclair ZX World](https://sinclairzxworld.com/viewtopic.php?p=49692#p49692){:target="_blank"} para descarga el juego.
