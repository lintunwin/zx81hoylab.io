---
layout: post
title:  "Frogger sfx"
month: "DIC"
img: "/assets/2022/01/frogger-sfx.jpg"
tags: ["2022", "Toddy Software"]
category: "Toddy Software"
---

El juego clásico de Cornsoft como siempre debería haber sido.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

El usuario del foro Sinclair ZX World, **kmurta**, ha añadido al juego Frogger de 1981 música y efectos de sonido.

Podéis oír el resultado en un vídeo que han subido
[ ](https://www.youtube.com/watch?v=C_GUdLvMHMQ){: .video target="_blank" style="background-image:url(../../../assets/play.png), url('https://img.youtube.com/vi/C_GUdLvMHMQ/0.jpg'"}

[Enlace al foro Sinclair ZX World](https://sinclairzxworld.com/download/file.php?id=10813&sid=1c5715b399f635884f14cfda0f7be2ab){:target="_blank"} para descarga el juego.