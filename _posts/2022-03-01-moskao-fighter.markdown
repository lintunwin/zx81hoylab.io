---
layout: post
title:  "Moskao Fighter"
month: "MAR"
img: "/assets/2022/03/moskao-fighter.jpg"
tags: ["2022", "Toddy Software"]
category: "Toddy Software"
---

Juego creado para la revista brasileña "Jogos 80".

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Ayuda a nuestro héroe a proteger a su colonia para repeler el ataque de voraces arañas asesinas en este excitante juego de zx81. Tiene soporte para sonido AY (ZONX-81) y para joysctick ZXpand

Es un port de un juego de 1988 publicado en el número 79 de la revista brasileña Micro Sistemas. Desarrollado en BASIC por Henrique Ávila Vianna para las microcomputadoras Apple II. Es un juego de disparos simple en el que tienes que disparar a arañas que descienden sobre sus telas y evitan que lleguen al suelo o te toquen.

Controla a Moskão (Big Fly), un guerrero que debe proteger heroicamente a su pueblo contra la invasión de un ejército de voraces arañas. Las arañas descienden a través de sus telas y el Moskão se defiende lanzando un ácido que corta la tela y derriba al invasor, pero inmediatamente otro toma su lugar no dando tranquilidad al valiente guerrero. Y para complicar las cosas, no puedes apuntar a la misma araña dos veces seguidas.

El Moskão se mueve de forma circular, es decir, cuando llega a un extremo de la pantalla reaparecerá en el lado opuesto, lo que le da una mayor posibilidad de llegar a las arañas más lejanas.

El juego tiene tres niveles de dificultad, donde con cada nivel la velocidad de las arañas aumenta, haciendo que la acción sea cada vez más dramática. Cada araña que cae aumenta la puntuación 10 veces el número de nivel y cada 1500 puntos ganas una vida extra.

{:refdef: style="text-align: center;"}
![Moskao Figther cover](/assets/2022/03/moskao-fighter_cover.jpg)
{: refdef}

Controles
- Izquierda  5 ( T,G,V,6,Y,H,N )
- Derecha    8 ( I,K,M,3,E,D,X )
- Disparo    0 ( P,NEWLINE,SPACE,1,Q,A )


Podéis oír el resultado en un vídeo que han subido el autor
[ ](https://www.youtube.com/watch?v=5lySBc00GGc){: .video target="_blank" style="background-image:url(../../../assets/play.png), url('https://img.youtube.com/vi/5lySBc00GGc/0.jpg'"}

[Enlace a itchio](https://toddysoftware.itch.io/moskao-fighter){:target="_blank"}
para su descarga.