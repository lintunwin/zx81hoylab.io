---
layout: post
title:  "Bootskell"
month: "AUG"
img: "/assets/2022/08/Bootskell.png"
tags: "2022 Inufuto"
category: Inufuto
---

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Tienes que eliminar a todos los monstruos para pasar de fase. Para acabar con un monstruo tienes que lanzarle un bloque, te sitúas pegado a un bloque y pulsas espacio, si el bloque esta en el borde o tiene otro pegado se rompe el bloque, si tiene espacio se lanza el bloque.

Los monstruos también lanzan bloques. Si te golpean ellos pierdes una vida, uno pierdes una vida si te toca un monstruo.

Tienes tiempo en cada fase, si acaba el tiempo pierdes una vida.
Si pierdes una vida en una pantalla solo quedaran los monstruos que no hayas matado y todos los bloques se regeneran.

Podéis ver el juego en un vídeo que han subido el autor
[ ](https://www.youtube.com/watch?v=OSGBWiWFjls){: .video target="_blank" style="background-image:url(../../../assets/play.png), url('https://img.youtube.com/vi/OSGBWiWFjls/0.jpg'"}


Los controles son las siguiente teclas
* Q = arriba
* A = abajo
* O = izquierda
* P = derecha
* Espacio = empujar bloque
* Espacio o Enter = empezar partida

[Enlace a la web del autor](http://inufuto.web.fc2.com/8bit/bootskell/#zx81){:target="_blank"} donde puedes descargar el juego en formato P o wav para cargar en una máquina real.

Además tiene el juego para muchos otros sistemas.

[Enlace directo al juego](http://inufuto.web.fc2.com/8bit/zx81/bootskell.p "download"){:target="_blank"}

