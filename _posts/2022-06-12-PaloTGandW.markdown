---
layout: post
title:  "Palo T Game and Watch"
month: "JUN"
img: "/assets/2022/06/paloT_GameAndWatch.png"
tags: ["2022", "Salvacam"]
category: "Salvacam"
---

Adaptación de la Game & Watch Helmet al [paloverso.](https://spectrumcomputing.co.uk/entry/36495/ZX-Spectrum/Mr_Palo_T_en_Escape_de_Mundo_Palo){:target="_blank"}


{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Como en la [Game & Watch](https://www.mariowiki.com/Helmet_(Game_%26_Watch)){:target="_blank"} tienes que atravesar la puerta de la derecha, cuando este abierta, evitando que te golpeen, en este caso, los escudos. Cuando te golpeen 4 veces acabas el juego.


Los controles son  
* 5 = izquierda
* 8 = derecha

[Enlace del código.](https://salvacam.gitlab.io/asset/paloT_GameAndWatch.c){:target="_blank"}

[Enlace del juego.](https://salvacam.gitlab.io/asset/paloT_GameAndWatch.p){:target="_blank"}

Todos lo juegos que he realizado para ZX81 y ZX Spectrum, además del código fuente, se encuentran en [esta web](https://salvacam.gitlab.io/){:target="_blank"} y se puede probar desde la web (excepto el único juego de ZX80, no he encontrado un emulador de ZX80 en javascript).