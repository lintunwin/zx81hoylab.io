---
layout: post
title:  "Shrooms"
month: "DIC"
img: "/assets/2021/12/shrooms.jpg"
tags: ["2021", "Bob's Stuff"]
category: "Bob's Stuff"
---

La interpretación de Bob Smith del clásico arcade Centipede para el ZX81

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Este proyecto lo ha tenido parado durante unos años, debido a que el desarrollo del juego Melkhior's Mansion le absorbía la mayor parte del tiempo. Recuerdo que hace meses puso en video del juego en movimiento.

Por suerte prometio que lo terminaría antes del 2021 y ha cumplido su palabra

{:refdef: style="text-align: center;"}
![Instrucciones](/assets/2021/12/shrooms_ins.jpg)
{: refdef}

Poco se puede añadir que no se sepa de este juego, una versión muy frenética, las primeras pantallas son relativamente fácil, pero ha medida que avanzas se van añadiendo nuevos enemigos que hace más difícil la misión de acabar con todas las partes del ciempies. 

Un juego con un acabado final muy bueno, como nos tiene acostumbrado el autor. Quien no conozca el autor que vea las joyas que tiene en su [web](https://bobs-stuff.itch.io){:target="_blank"} para ZX81 y ZX Spectrum.

[Enlace a itchio](https://bobs-stuff.itch.io/shrooms){:target="_blank"}
para su descarga.