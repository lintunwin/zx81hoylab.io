---
layout: post
title:  "Tee-Time"
month: "JUL"
img: "/assets/2022/07/TeeTime.png"
tags: ["2022", "XavSnap"]
category: "XavSnap"
---

Un juego de golf.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

[XavSnap](http://zx81.ordi5.free.fr/xavsnap/){:target="_blank"} ha transcrito este juego publicado el código por Eric LECHAUDEL en la revista francesa [Hebdogiciel](http://download.abandonware.org/magazines/Hebdogiciel/hebdogiciel_numero168/06.jpg){:target="_blank"}, de enero de 1987.

Es un juego de golf en el que puedes jugar en seis hoyos distintos. Para seleccionar hoyo deja que vayan mostrando antes de pulsar una tecla, el último mostrado será al que jugaras.

Una vez en el hoyo con L te muestra la leyenda del mapa y con A seleccionas el ángulo y fuerza del golpe.

Cuando llegues al green solo tendrás que seleccionar la fuerza.


[Enlace al foro Sinclair ZX World](https://sinclairzxworld.com/viewtopic.php?p=47163&sid=a3d854fc53c94164daf78b4af5a4a26d#p47163){:target="_blank"} para descarga el juego.