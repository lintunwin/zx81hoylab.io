---
layout: post
title:  "Wormhole"
month: "FEB"
img: "/assets/2023/02/Wormhole.png"
tags: ["2023", "Kurt-Arne Johnsen"]
category: "Kurt-Arne Johnsen"
---

Un nuevo juego compartido en el grupo de facebook [Keeping the Sinclair ZX80 and ZX81 Alive](https://www.facebook.com/1166498670/videos/580405407235495/){:target="_blank"}

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Un juego parecido al juego de la serpiente. Hay que recoger todos los dólares '$', obtienes un 1 punto por cada uno que cojas, para terminar el nivel. Debes evitar tu propio rastro y todas las minas '*'. Si pasas por un agujero de gusano, los bloques negros, obtendrás 10 puntos, pero no sabes por cuál agujero de gusano saldrás.

Los controles son las siguientes teclas:
* Q = arriba
* A = abajo
* O = izquierda
* P = derecha

El juego está en github en formato p.

[Repositorio de github](https://github.com/Retronissen/Sinclair-ZX81-Software-in-hounor-for-Sir-Clive){:target="_blank"}
para su descarga.

[Enlace directo](https://github.com/Retronissen/Sinclair-ZX81-Software-in-hounor-for-Sir-Clive/raw/4fc706435358c840ef3c804dac790ca9f83f53bb/wormhole.p){:target="_blank"} al archivo .p
