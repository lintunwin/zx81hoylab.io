---
layout: post
title:  "WWII Gunman y Orions Belt"
month: "DIC"
img: "/assets/2022/01/orionsBelt.jpg"
tags: ["2021", "2022", "Kurt-Arne Johnsen"]
category: "Kurt-Arne Johnsen"
---

El último juego del 2021 y el primero de 2022

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Los ha publicado el usuario en el grupo de facebook [ZX81 Owners Club](https://www.facebook.com/groups/ZX81OwnersClub/about){:target="_blank"}

**Orions Belt**

Primer juego del 2022 para el ZX81, lo ha liberado el autor pasada la media noche del día 1 de enero.

Un juego de dados multijugador, de 1 a 8 jugadores. Tienes 10 rondas donde lanzas tres dados, en cada ronda puedes lanzar tres veces, la primera vez los tres dados, y las otras dos veces siguientes eliges que dados lanzar. Tienes que conseguir tener el mismo numero en los tres dados, si lo consigues puntúas suma de los tres dados, menos si sacas tres unos que puntúas 21.
Al final de las 10 rondas gana el que más punto haya conseguido

{:refdef: style="text-align: center;"}
![WWII Gunman](/assets/2022/01/gunman.jpg)
{: refdef}

**WWII Gunman**

Este juego fue publicado el 29 de diciembre, y si no se me ha pasado ninguno es el último juego del año 2021 para el ZX81.

En el juego manejas un sistema antiaéreo de la segunda guerra mundial, tienes que derribar 10 aviones diferentes, que te disparan si te mueves lento. Va aumentando en dificultad durante el juego. Los aviones que dan más puntos te disparan con más frecuencia.

Los dos juegos se encuentra en un [repositorio de github](https://github.com/Retronissen/Sinclair-ZX81-Software-in-hounor-for-Sir-Clive){:target="_blank"}
para su descarga.