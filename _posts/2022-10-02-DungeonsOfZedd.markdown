---
layout: post
title:  "Dungeons of Zedd"
img: "/assets/2022/10/dungeons_of_zedd_beta_2.png"
tags: ["2022", "Walter Rissi"]
category: "Walter Rissi"
---

Un juego de mazmorras.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

El juego consiste en recorrer miles de cámaras y túneles para encontrar 10 reyes calaveras y acabar con ellos.

La mazmorra cambia cada vez que se juega. Tiene dos modos de control:

- usando OPQAM, Cursor o ZXPand Joystick, presiona fuego y una dirección para lanzar un hechizo en esa dirección.

- usando AWSD/JIKL, puedes moverte y lanzar hechizos en diferentes direcciones al mismo tiempo.

En las mazmorras hay diferentes tipos de enemigos. Tendrás que acabar con ellos para tener aumentar la experiencia, siendo esto necesario para atacar a cada rey calavera. No puedes atacar al rey calavera hasta que hayas ganado el nivel de experiencia requerido, lo que puede tenga hacer que vuelvas a una mazmorra anterior, ya que en la mazmorra donde hay un rey calavera no puedes matar al resto de enemigos.

Puede encontrar comida, armadura y pociones. Tienes que tener cuidado con las pociones porque algunas son curativas, pero otras reducen tu vida.

Al matar a todos los enemigos en una mazmorra puede que aparezcan nuevos objetos, pero también reaparecerán todos los enemigos de la mazmorra.


Podéis ver el juego en un vídeo que han subido el autor
[ ](https://www.youtube.com/watch?v=SYUzXtxVOUc){: .video target="_blank" style="background-image:url(../../../assets/play.png), url('https://img.youtube.com/vi/SYUzXtxVOUc/0.jpg'"}

[Enlace a itchio](https://naranjitogames.itch.io/sinclair-zx81-dungeons-of-zedd){:target="_blank"}

