---
layout: post
title:  "DitherPaint81"
month: "NOV"
img: "/assets/2021/11/ditherpaint81.jpg"
tags: ["2021", "Pixelmaker04"]
category: "Pixelmaker04"
---

No solos salen juegos en el 2021 para el ZX81 también hacen aplicaciones.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

En esta aplicación podemos dibujar en el ZX81 e imprimir el resultado.

{:refdef: style="text-align: center;"}
![Resultado](/assets/2021/11/ditherpaint81_salida.jpg)
{: refdef}

El programa es un port del programa DITHERPAINT99 para el microordenador TI99.

Moviendo el cursor con las teclas WASD y con el punto pintas el caracter que tengas seleccionado, cambias el caracter seleccionado con los números.

En el enlace de itch.io incluye además del archivo P una guía de uso en txt y pdf.

En redes sociales el autor ha mostrado fotos del uso del programa en hardware real y ha puesto a la venta edición física através de ebay.

{:refdef: style="text-align: center;"}
![Hardware real](/assets/2021/11/ditherpaint81_real.jpg)
{: refdef}

[Enlace a itchio](https://pixelmaker04.itch.io/ditherpaint81){:target="_blank"}
