---
layout: post
title:  "Spider Mutants"
month: "OCT"
img: "/assets/2021/10/spider-mutant.jpg"
tags: ["2021", "Toddy Software"]
category: "Toddy Software"
---

La primera entrada no es un juego nuevo es del año 1985, de P.Wailand, tenia un error que se hacia injugable, según indica *kmurta*, el usuario del foro Sinclair ZX World que lo ha arreglado, posiblemente es un error generado durante la conversión de la cinta.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Es un sencillo matamarciano muy adictivo, tienes que salvar la tierra de una invasión alienígena, tienes 5 naves para defender los ataques de las naves arañas, en realidad es sola una pero se ira reponiendo a medida que la destruya.
La naves arañas pueden disparar y lanzar minas que reducirán la posibilidad de movimiento, además la pantalla esta llena de bloques que sirven como escudos tanto para el jugador como el enemigo, y otras naves más pequeña recorren la pantalla de lado a lado, cada cierto tiempo, disparando.

Tiene una cuidad presentación y el juego se mueve a una buena velocidad, a medida que avanzas de nivel se va incrementando los ataques de los enemigos.

[Enlace de descarga de la versión en idioma Ingles](https://www.sinclairzxworld.com/download/file.php?id=10353)

[Enlace al foro Sinclair ZX World](https://www.sinclairzxworld.com/viewtopic.php?f=4&t=4401){:target="_blank"}
donde hay más detalles del error y los enlaces de descarga en Ingles y Alemán.
