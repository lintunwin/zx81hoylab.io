---
layout: post
title:  "Falken's Maze y ZX81 Slicer"
month: "Mar"
img: "/assets/2023/03/falkens.png"
tags: ["2023", "Adrian Pilkington"]
category: "Adrian Pilkington"
---

Dos nuevos juegos para 1k.

**Falken's Maze**

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Aunque el nombre se inspira en la película Juegos de guerra, el juego no tiene mucha relación con la película.

Debes llegar abajo de la pantalla evitando los bloques negros. Puedes usar las bombas para abrir camino, pero son limitadas

[ ](https://www.youtube.com/watch?v=PTESewITiWw){: .video target="_blank" style="background-image:url(../../../assets/play.png), url('https://img.youtube.com/vi/PTESewITiWw/0.jpg'"}

Los controles son:   
* Z = izquierda
* M = derecha
* N = abajo
* B = poner bomba

[Código fuente del juego](https://github.com/AdrianPilko/ZX81FalkensMaze){:target="_blank"}

Se pueden descargar los juegos del [repositorio de github](https://github.com/AdrianPilko/ZX81FalkensMaze/blob/main/falkens1k.p){:target="_blank"}
o en [itchio](https://adrianpilko.itch.io/zx81-falkens-maze){:target="_blank"}

**ZX81 Slicer**

{:refdef: style="text-align: center;"}
![slicer](/assets/2023/03/slicer.png)
{: refdef}

Debes llegar abajo de la pantalla evitando las barras en constante movimiento.

[ ](https://www.youtube.com/watch?v=Gk2FD2sE03E){: .video target="_blank" style="background-image:url(../../../assets/play.png), url('https://img.youtube.com/vi/Gk2FD2sE03E/0.jpg'"}

Los controles son:   
* Z = izquierda
* M = derecha
* N = abajo

[Código fuente del juego](https://github.com/AdrianPilko/ZX81SlicerGame1K){:target="_blank"}

Se pueden descargar los juegos del [repositorio de github](https://github.com/AdrianPilko/ZX81SlicerGame1K/blob/main/slicer1k_foremu.p){:target="_blank"}
o en [itchio](https://adrianpilko.itch.io/zx81-slicer){:target="_blank"}