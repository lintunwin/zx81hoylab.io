---
layout: post
title:  "Minehunt y Santa Simulator 1.0"
month: "DIC"
img: "/assets/2021/12/minehunt.jpg"
tags: ["2021", "Kurt-Arne Johnsen"]
category: "Kurt-Arne Johnsen"
---

Dos nuevos juegos que han aparecido en el grupo de facebook [ZX81 Owners Club](https://www.facebook.com/groups/ZX81OwnersClub/about){:target="_blank"}

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Una versión Mined-Out, o Campo Minado como llego a España, que ha realizado en basic. No por eso se mueve lento, todo lo contrario, se muy muy fluido, solo cuando chocas con una mina tarda unos segundos en mostrar las minas en pantalla.
Las minas se colocan aleatoriamente (ocultas) y aumentan en número por cada nivel que completes. La única información que obtienes durante el juego es cuántas bombas hay cerca de ti.

{:refdef: style="text-align: center;"}
![Santa Simulator 1.0](/assets/2021/12/santa.jpg)
{: refdef}

Eres Santa y tienes que colocar a Rudolph y el trineo y tirar los regalos para que entren en la chimenea. A mayor altitud más puntos consigues.
Bastante difícil de que entren los regalos por la chimenea.

Los dos juegos están en github en formato p y z81, así como el código fuente de los dos juegos

[Repositorio de github](https://github.com/Retronissen/Sinclair-ZX81-Software-in-hounor-for-Sir-Clive){:target="_blank"}
para su descarga.