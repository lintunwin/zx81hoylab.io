---
layout: post
title:  "Space Invaders for ZX81"
month: "DIC"
img: "/assets/2021/12/SpaceInvadersforZX81.jpg"
tags: ["2021", "SplinterGU"]
category: "SplinterGU"
---

Space Invaders para el ZX81

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Space Invaders con gráficos en alta resolución, sonido por la extensión Zon-X81 y soporte Chroma81

Necesita expansión de 32K o zxpand

{:refdef: style="text-align: center;"}
![Version Chroma](/assets/2021/12/SpaceInvadersforZX81_chroma.jpg)
{: refdef}

Su código fuente está disponible en la página de ithcio del autor

[Enlace a itchio](https://splintergu.itch.io/space-invaders-for-zx81-wrx){:target="_blank"}
para su descarga.
