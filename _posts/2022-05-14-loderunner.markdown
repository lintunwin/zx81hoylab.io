---
layout: post
title:  "Loderunner"
month: "MAY"
img: "/assets/2022/05/loderunner.jpg"
tags: ["2022", "Dr Beep"]
category: "Dr Beep"
---

Una versión de [Loderunner](https://es.wikipedia.org/wiki/Lode_Runner){:target="_blank"} para ZX81 con 16K.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

En 2017 [David Stephenson](https://www.zx81keyboardadventure.com/){:target="_blank"} empezó una versión de Loderunner.
Con los niveles de David Stephenson, Dr Beep ha realizado el juego. Ha usado un método de comprensión que permite 75 niveles en un ZX81 de 16K. Y hay dos partes por lo que tenemos 150 niveles distintos donde recoger oro evitando a los guardias.

El menú se puede definir los controles con la tecla R. Y se comienza la partida con la tecla S.

Puedes saltar un nivel con SHIFT+N. Cuando estás encerrado y no puedes salir puedes terminar una vida con SHIFT+R.

Obtendrás una vida extra al terminar cada nivel.

[Enlace al foro Sinclair ZX World](https://sinclairzxworld.com/viewtopic.php?p=46710#p46710){:target="_blank"} para descarga el juego
