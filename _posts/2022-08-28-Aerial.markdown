---
layout: post
title:  "Aerial"
month: "AGO"
img: "/assets/2022/08/Aerial.png"
tags: "2022 Inufuto"
category: Inufuto
---

Un juego de nave con scroll horizontal.

{:refdef: style="text-align: center;"}
![{{page.title}}]({{page.img}})
{: refdef}

Debes evitar las colisionar con todas las naves enemigas y sus disparos, así como chocar con el suelo, porque perdarás una vida. El juego se mueve continuamente en horizontal hacia la derecha, debes tener cuidado ya que por la izquierda aparecen naves enemigas. 

Al final de cada fase tienes un enemigo final.  

Al destruir las naves enemigas que suben verticalmente es probable que dejen una vida extra.

Podéis ver el juego en un vídeo que han subido el autor
[ ](https://www.youtube.com/watch?v=ExMuRohHbUE){: .video target="_blank" style="background-image:url(../../../assets/play.png), url('https://img.youtube.com/vi/ExMuRohHbUE/0.jpg'"}


Los controles son las siguiente teclas
* Q = arriba
* A = abajo
* O = izquierda
* P = derecha
* Espacio = disparar
* Espacio o Enter = empezar partida

[Enlace a la web del autor](http://inufuto.web.fc2.com/8bit/aerial/#zx81){:target="_blank"} donde puedes descargar el juego en formato P o wav para cargar en una máquina real.

Además tiene el juego para muchos otros sistemas.

[Enlace directo al juego](http://inufuto.web.fc2.com/8bit/zx81/aerial.p "download"){:target="_blank"}

