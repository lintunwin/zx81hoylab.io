---
layout: default
title: About
---
# Sobre el blog

## Blog sobre nuevos desarrollos para del ordenador Sincliar ZX81.

Comencé este blog en octubre del 2021 debido a que es bastante lioso el estar al día de las novedades que iban saliendo para el ZX81. 

La mayoría de los novedades suelen publicarse en el foro [Sinclair ZX World](https://www.sinclairzxworld.com/index.php){:target="_blank"} pero otras muchas los autores lo publicaban en redes sociales o plataformas de distribución. 

Decidi tener todo centralizado y que estuviera disponible para cualquier interesado.

El blog esta creado usando el generador de sitios web estáticos [Jekyll](https://jekyllrb.com/){:target="_blank"} y se despliega en [GitLab Pages.](https://gitlab.com/pages){:target="_blank"}

Todo el código del blog tiene licencia de código abierto, esta disponible en [GitLab](https://gitlab.com/zx81hoy/zx81hoy.gitlab.io){:target="_blank"}

Esta web no tiene cookie ni recopila datos.

Si quieres ponerte en contacto conmigo puedes mandarme un correo al email que hay en el pie, recuerda modificar el enlace.

Tengo otra web, que te puede interesar, con [mis juegos para ZX Spectrum y ZX81](https://salvacam.gitlab.io){:target="_blank"}